﻿using System.Collections.Generic;
using UnityEngine;

public class FruitManager : MonoBehaviour
{
    public ScrollController Scroller;
    public EnergyManager Energy;
    public Transform SpawnPosition;

    public GameObject FruitPrefab;

    public Sprite[] FruitSprites;

    [Header("Config")]
    [SerializeField] private float fruitInterval = 3f;

    [SerializeField] private float fruitRandom = 6f;

    [SerializeField] private float fruitEnergyBase = 0.1f;
    [SerializeField] private float fruitEnergyRandom = 0.2f;

    [SerializeField] private float fruitSpawnHeightRandom = 1f;

    [Header("State")]
    [SerializeField] private float fruitRemainingTime = 3f;

    public List<GameObject> Fruits = new List<GameObject>();

    private void Awake()
    {
        fruitRemainingTime += Random.Range(0f, 3f);
    }

    private void Update()
    {
        fruitRemainingTime -= Time.deltaTime;
        if (fruitRemainingTime <= 0.0f)
        {
            CreateFruit();
            fruitRemainingTime = fruitInterval + Random.Range(0.0f, fruitRandom);
        }
    }

    private void CreateFruit()
    {
        // Random Y Pos
        float h = Random.Range(0f, fruitSpawnHeightRandom);
        var pos = SpawnPosition.position; pos.y += h;

        // Create
        var go = GameObject.Instantiate(FruitPrefab, pos, Quaternion.identity, SpawnPosition);

        // set manager for callback
        FruitPickup fP = go.GetComponent<FruitPickup>();
        fP.Manager = this;
        fP.EnergyValue = fruitEnergyBase + Random.Range(0f, fruitEnergyRandom);

        // set sprite
        Sprite s = FruitSprites[Random.Range(0, FruitSprites.Length)];
        var sr = go.GetComponent<SpriteRenderer>();
        sr.sprite = s;

        // Collect
        Fruits.Add(go);
        Scroller.ScrollingElements.Add(go.GetComponent<ScrollingElement>());
    }

    public void AddEnergyFromFruit(FruitPickup _fruit)
    {
        Energy.AddEnergy(_fruit.EnergyValue);
    }

    private void OnDrawGizmosSelected()
    {
        // Right Border
        Vector3 start = SpawnPosition.position;
        Vector3 end = start; end.y += fruitSpawnHeightRandom;
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(start, end);
    }
}