﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EnergyManager : MonoBehaviour
{
    public Slider slider;

    public float DecreaseMultiplier = 0.01f;

    public UnityEvent OnEnergyEmpty = new UnityEvent();

    [Range(0f, 1f)]
    public float CurrentEnergy = 1f;

    public bool IsEnded = false;

    private void Update()
    {
        if (IsEnded == true) return;

        CurrentEnergy = CurrentEnergy - Time.deltaTime * DecreaseMultiplier;
        slider.value = Mathf.Clamp01(CurrentEnergy) * slider.maxValue;
        if (CurrentEnergy <= 0.0f)
        {
            OnEnergyEmpty.Invoke();
            IsEnded = true;
        }
    }

    public void AddEnergy(float _energyPercent = 0.1f)
    {
        CurrentEnergy = Mathf.Min(CurrentEnergy + _energyPercent, 1f);
    }

    public void RemoveEnergy(float _energyPercent = 0.1f)
    {
        CurrentEnergy = Mathf.Max(CurrentEnergy - _energyPercent, 0f);
    }
}