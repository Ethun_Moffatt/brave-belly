﻿using UnityEngine;

public class RepeatingElement : MonoBehaviour
{
    private SpriteRenderer SpriteRenderer;

    public float XOffset = 0f;

    public float WidthOffset = 0f;

    private void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    public float GetWidth()
    {
        if (!SpriteRenderer) return 0f;
        return SpriteRenderer.bounds.size.x + WidthOffset;
    }

    public float GetExtent()
    {
        return GetWidth() * 0.5f;
    }

    private void OnDrawGizmos()
    {
        var sr = GetComponent<SpriteRenderer>();
        if (!sr) return;

        var b = sr.bounds;
        var s = b.size; s.x += WidthOffset;
        var c = b.center; c.x += XOffset;

        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(c, s);
    }
}