﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DamageEvent : UnityEvent<Hurdle> { }

public class PlayerController : MonoBehaviour
{
    [Header("References")]
    public ParticleSystem LandingParticles = null;

    private Transform PlayerTransform = null;
    private SpriteRenderer PlayerSprite = null;
    private Animator PlayerAnimator = null;
    private Rigidbody2D PlayerRigid = null;
    private CapsuleCollider2D PlayerCapsule = null;

    private string HAxis_Control = "Horizontal";
    private string VAxis_Control = "Vertical";
    private string Jump_Control = "Fire1";
    private string Player_Layer = "Player";
    private string Ground_Layer = "Ground";

    [Header("Config")]
    public bool DrawDebug = false;

    [Header("Walk")]
    public float Acceleration = 5f;

    public float MaxSpeed = 10f;

    public float RightBorderX = 8.5f;

    [Header("Ground Detection")]
    [Range(0f, 1f)]
    public float GroundSizeModifier = 0.95f;

    [Range(0f, 1f)]
    public float GroundHeightModifier = 0.95f;

    public Vector2 GroundOffset = Vector2.zero;

    [Header("Jump")]
    public float JumpVelocity = 5f;

    [Range(0f, 2f)]
    public float JumpCooldown = 0.25f;

    [Range(1f, 10f)]
    public float FallMultiplier = 0f;

    [Range(1f, 10f)]
    public float LowJumpMultiplier = 0f;

    [Header("State")]
    public Vector2 UserInput = Vector2.zero;

    public bool JumpInput = false;
    public bool Grounded = true;
    public bool IsJumping = false;
    public float JumpCooldownCurrent = 0.0f;

    [HideInInspector] public UnityEvent OnJump = new UnityEvent();
    [HideInInspector] public UnityEvent OnLanding = new UnityEvent();
    [HideInInspector] public DamageEvent OnDamaged = new DamageEvent();

    private void Awake()
    {
        PlayerTransform = this.transform;
        PlayerSprite = this.GetComponentInChildren<SpriteRenderer>();
        PlayerAnimator = this.GetComponentInChildren<Animator>();
        PlayerRigid = this.GetComponent<Rigidbody2D>();
        PlayerCapsule = this.GetComponent<CapsuleCollider2D>();
    }

    private void Update()
    {
        UserInput.x = Input.GetAxis(HAxis_Control);
        UserInput.y = Input.GetAxis(VAxis_Control);

        JumpInput = Input.GetButtonDown(VAxis_Control);
        JumpInput = Input.GetButton(VAxis_Control);

        // Check Grounded
        Collider2D other_collider = Physics2D.OverlapCircle(GroundCirclePosition(), GroundCircleRadius(), LayerMask.NameToLayer(Ground_Layer));
        bool bottomHit = other_collider != null;

        Grounded = !PlayerCapsule || bottomHit;

        // Landing
        if (bottomHit && IsJumping)
        {
            IsJumping = false;
            PlayerAnimator.SetBool("IsJumping", IsJumping);
            OnLanding.Invoke();

            if (PlayerRigid.velocity.y < -22f) // Hard Landing
                LandingParticles.Emit(100);
        }
    }

    private void FixedUpdate()
    {
        // Jump Cooldown
        if (JumpCooldownCurrent > 0f)
            JumpCooldownCurrent -= Time.fixedDeltaTime;

        // Gravity
        if (JumpInput && Grounded && JumpCooldownCurrent <= 0f)
        {
            IsJumping = true;
            PlayerAnimator.SetBool("IsJumping", IsJumping);
            JumpCooldownCurrent = JumpCooldown;
            PlayerRigid.velocity += Vector2.up * JumpVelocity;
            Grounded = false;
            // Event
            OnJump.Invoke();
        }

        // Faster falling
        if (PlayerRigid.velocity.y < 0)
            PlayerRigid.velocity += Vector2.up * Physics.gravity.y * (FallMultiplier - 1) * Time.fixedDeltaTime;

        // Control jump height by length of time jump button held
        if (PlayerRigid.velocity.y > 0)
            PlayerRigid.velocity += Vector2.up * Physics.gravity.y * Time.fixedDeltaTime * ((!JumpInput) ? (LowJumpMultiplier - 1) : 1f);

        // Restrict Walk
        if (PlayerTransform.position.x > RightBorderX) return;

        // Walk
        float xSpeed = Mathf.Min(UserInput.x * Acceleration * Time.fixedDeltaTime, MaxSpeed);
        PlayerRigid.velocity = new Vector2(xSpeed, PlayerRigid.velocity.y);
    }

    private Vector3 GroundCirclePosition()
    {
        float radius = GroundCircleRadius();

        float height =
              PlayerCapsule.offset.y
            - (PlayerCapsule.size.y * 0.5f) + radius
            - (1f - GroundHeightModifier) * radius;

        return this.transform.position
            + new Vector3(PlayerCapsule.offset.x, height, 0f)
            + new Vector3(GroundOffset.x, GroundOffset.y, 0f);
    }

    private float GroundCircleRadius()
    {
        return (PlayerCapsule.size.x * 0.5f) - ((1f - GroundSizeModifier) * (PlayerCapsule.size.x * 0.5f));
    }

    private void OnDrawGizmos()
    {
        if (!DrawDebug) return;

        if (PlayerCapsule)
        {
            Gizmos.color = Grounded ? Color.green : Color.cyan;
            Gizmos.DrawWireSphere(GroundCirclePosition(), GroundCircleRadius());
        }

        // Right Border
        float heightExtent = 5f;
        Vector3 start = Vector3.right * RightBorderX; start.y += heightExtent;
        Vector3 end = Vector3.right * RightBorderX; end.y -= heightExtent;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(start, end);
    }
}

// public static class Vector3Extension
// {
//     public static float MaxElement(this Vector3 vector)
//     {
//         return Mathf.Max(Mathf.Max(vector.x, vector.y), vector.z);
//     }
// }