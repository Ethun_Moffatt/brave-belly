﻿using System.Collections.Generic;
using UnityEngine;

public class HurdlesManager : MonoBehaviour
{
    public ScrollController Scroller;
    public SpeedManager SpeedManager;
    public GameObject[] HurdlePrefabs;
    public Transform SpawnPosition;

    [Header("Config")]
    [SerializeField] private float hurdleInterval = 2f;

    [SerializeField] private float hurdleRandom = 1.5f;

    private float HardModeDistance = 60.0f;

    [Header("State")]
    [SerializeField] private float hurdleRemainingTime;

    public List<GameObject> Hurdles = new List<GameObject>();

    private void Start()
    {
    }

    private void Update()
    {
        hurdleRemainingTime -= Time.deltaTime;
        if (hurdleRemainingTime <= 0.0f)
        {
            CreateHurdle();
            hurdleRemainingTime = hurdleInterval + Random.Range(0.0f, hurdleRandom);
        }
    }

    private void CreateHurdle()
    {
        // Hard Mode
        int type = SpeedManager.CurrentDistance > HardModeDistance ? Random.Range(0, HurdlePrefabs.Length) : 0;

        GameObject prefab = HurdlePrefabs[type];
        // Create
        var go = GameObject.Instantiate(prefab, SpawnPosition.position, Quaternion.identity, SpawnPosition);

        // Set callback
        Hurdle h = go.GetComponent<Hurdle>();
        h.Manager = this;

        // Collect
        Hurdles.Add(go);
        Scroller.ScrollingElements.Add(go.GetComponent<ScrollingElement>());
    }

    public void HurdleHit()
    {
        SpeedManager.ReduceHurdleCleared();
    }

    public void HurdleCleared()
    {
        SpeedManager.IncreaseHurdleCleared();
    }
}