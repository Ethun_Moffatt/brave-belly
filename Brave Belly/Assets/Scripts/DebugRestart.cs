﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugRestart : MonoBehaviour
{
    private void Start()
    {
    }

    private void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            SceneManager.LoadScene(0);
        }
    }
}