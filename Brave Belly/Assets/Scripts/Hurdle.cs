﻿using UnityEngine;

public class Hurdle : MonoBehaviour
{
    public HurdlesManager Manager;
    public GameObject DamageParticles;
    private bool activated = false;
    private bool cleared = false;

    private void Update()
    {
        if (!cleared && !activated && Manager.SpeedManager.AllowScoring)
        {
            var player = GameObject.FindWithTag("Player");
            if (transform.position.x < (player.transform.position.x - 2f))
            {   
                cleared = true;
                Manager.HurdleCleared();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D _other)
    {
        if (!activated && _other != null)
            DoPlayerHit(_other.collider);
    }

    private void OnTriggerEnter2D(Collider2D _other)
    {
        if (!activated && _other != null)
            DoPlayerHit(_other);
    }

    private void DoPlayerHit(Collider2D _other)
    {
        bool hitPlayer = _other.gameObject.CompareTag("Player");
        if (hitPlayer)
        {
            activated = true;

            // Damage Event
            var em = _other.GetComponent<EnergyManager>();
            em.RemoveEnergy(0.05f);
            //
            SpawnParticles();
        }
    }

    private void SpawnParticles()
    {
        GameObject.Instantiate(DamageParticles, transform.position, Quaternion.identity, transform);
    }

    private void OnDestroy()
    {
        var se = GetComponent<ScrollingElement>();
        Manager.Scroller.ScrollingElements.Remove(se);

        Manager.Hurdles.Remove(gameObject);
    }
}