﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class EndGameTrigger : MonoBehaviour
{
    public UnityEvent OnEndGameBegin = new UnityEvent();
    public UnityEvent OnEndGameEnd = new UnityEvent();

    private void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.CompareTag("Player"))
            StartEndGame();
    }

    public void EndGame()
    {
        OnEndGameEnd.Invoke();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
    }

    public void StartEndGame()
    {
        OnEndGameBegin.Invoke();
    }
}