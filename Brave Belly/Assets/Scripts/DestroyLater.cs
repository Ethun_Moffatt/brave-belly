﻿using UnityEngine;

public class DestroyLater : MonoBehaviour
{
    public bool Enabled = false;
    public float TimeRemaining = 10f;

    private void Update()
    {
        if (Enabled)
            TimeRemaining -= Time.deltaTime;
        if (TimeRemaining < 0.0f)
            GameObject.Destroy(gameObject);
    }
}