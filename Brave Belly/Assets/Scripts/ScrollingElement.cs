﻿using UnityEngine;

public class ScrollingElement : MonoBehaviour
{
    public float SpeedMultiplier = 1f;

    public void Move(float _speedInput = 1f)
    {
        transform.Translate(Vector3.left * _speedInput * SpeedMultiplier * Time.smoothDeltaTime);
    }
}