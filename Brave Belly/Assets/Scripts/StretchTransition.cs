﻿using UnityEngine;
using UnityEngine.Events;

public class StretchTransition : MonoBehaviour
{
    public bool DoToggle = false;

    public float Duration = 3f;
    public AnimationCurve Curve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0f, 0f));

    [Header("State")]
    public float Remaining = 0f;

    public bool IsShowing = false;
    public bool IsTransitioning = false;

    public UnityEvent OnFinished = new UnityEvent();

    private void Awake()
    {
        SetState(IsShowing ? 1f : 0f);
    }

    private void Update()
    {
        if (DoToggle)
        {
            TransitionTo(!IsShowing);

            DoToggle = false;
        }

        if (IsTransitioning)
        {
            Remaining -= Time.deltaTime;
            if (Remaining <= 0.0f)
            {
                Remaining = 0.0f;
                IsTransitioning = false;
                OnFinished.Invoke();
            }

            float currentPercent = Remaining / Duration;
            float state = IsShowing ? 1f - currentPercent : currentPercent;
            float evaluation = Curve.Evaluate(state);
            SetState(evaluation);
        }
    }

    public void Toggle()
    {
        DoToggle = true;
    }

    public void TransitionTo(bool _isShowing)
    {
        IsShowing = _isShowing;

        IsTransitioning = true;
        Remaining = Duration;
    }

    public void SetState(float _percent)
    {
        var scale = transform.localScale; scale.x = _percent;
        transform.localScale = scale;
    }
}