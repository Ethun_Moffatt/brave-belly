﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScrollController : MonoBehaviour
{
    public List<ScrollingElement> ScrollingElements;

    public List<SpriteRenderer> RepeatingSprites;

    public List<SpriteRenderer> RepeatingRoads;

    public bool DrawDebug = true;
    public bool DoScroll = true;
    public bool DoRepeat = true;

    public float Speed = 2;
    [SerializeField] private float repeatBorderOffset = 0f;
    [SerializeField] private float debugRepeatBorderHeight = 10f;

    [SerializeField] private float RangeDistance = 0f;

    private void Awake()
    {
        ScrollingElements = new List<ScrollingElement>();
        ScrollingElements.AddRange(GetComponentsInChildren<ScrollingElement>());

        RepeatingSprites = GameObject.FindGameObjectsWithTag("Repeatable")
                    .Select(go => go.GetComponent<SpriteRenderer>())
                    .Where(sr => sr != null)
                    .OrderBy((sr) => sr.bounds.max.x)
                    .ToList();

        RepeatingRoads = GameObject.FindGameObjectsWithTag("Road")
            .Select(go => go.GetComponent<SpriteRenderer>())
            .Where(sr => sr != null)
            .OrderBy((sr) => sr.bounds.max.x)
            .ToList();

        UpdateRange();
    }

    private void Update()
    {
        // Scrolling
        if (DoScroll)
        {
            for (int i = ScrollingElements.Count - 1; i >= 0; i--)
            {
                var sElement = ScrollingElements[i];
                if (sElement == null)
                {
                    ScrollingElements.RemoveAt(i);
                    continue;
                }
                sElement.Move(Speed);
            }
        }

        // Repeating
        if (DoRepeat)
        {
            // Road
            for (int i = RepeatingRoads.Count - 1; i >= 0; i--)
            {
                var rElement = RepeatingRoads[i];
                if (rElement == null)
                {
                    RepeatingSprites.RemoveAt(i);
                    continue;
                }
                if (rElement.bounds.max.x <= transform.position.x + repeatBorderOffset)
                {
                    // Set Pos
                    var pos = rElement.transform.position; pos.x += RangeDistance;
                    rElement.transform.position = pos;

                    // Sort
                    RepeatingRoads.Sort((a, b) => a.bounds.max.x.CompareTo(b.bounds.max.x));
                }
            }

            // Sprites
            for (int i = RepeatingSprites.Count - 1; i >= 0; i--)
            {
                var rElement = RepeatingSprites[i];
                if (rElement == null)
                {
                    RepeatingSprites.RemoveAt(i);
                    continue;
                }

                if (rElement.bounds.max.x <= transform.position.x + repeatBorderOffset)
                {
                    // Set Pos
                    var pos = rElement.transform.position; pos.x += RangeDistance;
                    rElement.transform.position = pos;

                    // Sort
                    RepeatingSprites.Sort((a, b) => a.bounds.max.x.CompareTo(b.bounds.max.x));
                }
            }
        }
    }

    public float GetWidth(SpriteRenderer _sr)
    {
        return _sr.bounds.size.x;
    }

    public float GetExtent(SpriteRenderer _sr)
    {
        return _sr.bounds.extents.x;
    }

    private void UpdateRange()
    {
        RangeDistance = Mathf.Abs(
              RepeatingRoads.FirstOrDefault().bounds.min.x
            - RepeatingRoads.LastOrDefault().bounds.max.x);
    }

    private void OnDrawGizmos()
    {
        if (!DrawDebug) return;

        Vector3 start, end;

        // Repeat Border
        start = transform.position;
        start.x += repeatBorderOffset;
        start.y += debugRepeatBorderHeight;

        end = transform.position;
        end.x += repeatBorderOffset;
        end.y -= debugRepeatBorderHeight;

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(start, end);

        // Self
        start = transform.position;
        start.y += debugRepeatBorderHeight;

        end = transform.position;
        end.y -= debugRepeatBorderHeight;

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(start, end);
    }
}