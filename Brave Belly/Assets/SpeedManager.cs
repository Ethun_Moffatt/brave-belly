﻿using TMPro;
using UnityEngine;

public class SpeedManager : MonoBehaviour
{
    public ScrollController Scroller;

    public float MinSpeed = 1f;
    public float MaxSpeed = 2f;
    public float SpeedUpDuration = 20f;
    public AnimationCurve Curve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));

    public float Progress = 0f;
    public bool IsSpeedingUp = true;

    public TextMeshProUGUI DistanceText;
    public bool AllowScoring = true;
    public float CurrentDistance = 0f;

    private void Awake()
    {
        PlayerScoreData.Hurdles = 0;
        PlayerScoreData.Distance = 0f;
    }

    private void Update()
    {
        UpdateDistance();

        if (!IsSpeedingUp) return;

        if (Progress < SpeedUpDuration)
        {
            Progress += Time.deltaTime;
            UpdateSpeed();
        }
        else
        {
            Progress = SpeedUpDuration;
            UpdateSpeed();
            IsSpeedingUp = false;
            // Finished SpeedingUp
        }
    }

    private void UpdateSpeed()
    {
        if (!Scroller) return;

        Scroller.Speed = Mathf.Lerp(MinSpeed, MaxSpeed, Curve.Evaluate(Progress / SpeedUpDuration));
    }

    private void UpdateDistance()
    {
        if (AllowScoring)
            CurrentDistance += Time.deltaTime;
        // UI
        DistanceText.text = string.Format("{0:0.0}", CurrentDistance);
    }

    public void StopScoring()
    {
        AllowScoring = false;
    }

    public void IncreaseHurdleCleared()
    {
        PlayerScoreData.Hurdles += 1;
    }

    public void ReduceHurdleCleared()
    {
        PlayerScoreData.Hurdles = Mathf.Max(PlayerScoreData.Hurdles - 1, 0);
    }

    public void SaveScore()
    {
        PlayerScoreData.Distance = CurrentDistance;
        PlayerScoreData.Score = PlayerScoreData.Distance + (PlayerScoreData.Hurdles * 10f);
    }
}