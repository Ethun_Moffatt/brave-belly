﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class EndGameManager : MonoBehaviour
{
    public UnityEvent OnTransitionEnd = new UnityEvent();

    public UnityEvent OnCutToBlackStart = new UnityEvent();


    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI HurdlesText;
    public TextMeshProUGUI DistanceText;

    public AudioSource Footsteps;
    public AudioSource Gunshot;
    public Animator CameraAnimator;
    public Animator PlayerAnimator;

    private void Awake()
    {
        UpdateScoreTexts();
    }

    private void Update()
    {
    }

    public void StartEvent()
    {
        OnTransitionEnd.Invoke();   
        //StartCoroutine(Wait());
        //StartCoroutine(WaitSecond());
    }

    public void StartCameraEvent()
    {
        CameraAnimator.SetTrigger("Start");
    }

    public void StartFootsteps()
    {
        Footsteps.Play();
    }

    public void StartGunshot()
    {
        Gunshot.Play();
    }

    public void StartPlayerDeath()
    {
        PlayerAnimator.SetTrigger("Dead");
    }

    public void StartEndTransition()
    {
        OnCutToBlackStart.Invoke();
    }

    //private IEnumerator Wait()
    //{
    //    yield return new WaitForSecondsRealtime(6);
    //    OnWaitEnd.Invoke();
    //}

    //private IEnumerator WaitSecond()
    //{
    //    yield return new WaitForSecondsRealtime(10);
    //    OnWaitEnd.Invoke();
    //}

    private void UpdateScoreTexts()
    {
        ScoreText.text = string.Format("{0:0.0}", PlayerScoreData.Score);
        HurdlesText.text = PlayerScoreData.Hurdles.ToString();
        DistanceText.text = string.Format("{0:0.0}", PlayerScoreData.Distance);
    }
}