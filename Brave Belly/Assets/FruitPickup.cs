﻿using UnityEngine;

public class FruitPickup : MonoBehaviour
{
    public FruitManager Manager;
    public float EnergyValue = 0.1f;

    private void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.CompareTag("Player"))
        {
            if (!Manager) return;

            Manager.AddEnergyFromFruit(this);
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        var se = GetComponent<ScrollingElement>();
        Manager.Scroller.ScrollingElements.Remove(se);

        Manager.Fruits.Remove(gameObject);
    }
}